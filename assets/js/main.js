// external js: isotope.pkgd.js


// init Isotope
// $('.grid').isotope({ 
// 	filter: '.all-pkg',
// 	itemSelector: '.grid-item', 
// 	layoutMode: 'fitRows' 
// });
$(window).resize(function(){location.reload();});
// init Isotope
var $grid = $('.grid').isotope({
  // options
});
$('.btn-adddis').click(function(e){
  e.preventDefault();
  $(this).next('.discount-popup').toggle();
});
// filter items on button click
$('.filter-group').on( 'click', 'a', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});
$(".del-btn").on('click', function(){
  $(".del-options").addClass('active');
  $(".del-options").show(1000);
});
$(".input-custom-style").fileinput({
	'showRemove': false,
	'showUpload': false,
	'showPreview': true,
	'browseLabel': 'CHOOSE FILE',
	'browseIcon': ' ',
	'showUploadedThumbs': false,
	'showClose': false
});
$(".input-add").fileinput({
	'showRemove': false,
	'showUpload': false,
	'showPreview': false,
	'browseLabel': 'CHOOSE FILE',
	'browseIcon': ' ',
	'showUploadedThumbs': false,
	'showClose': false
})
$("#date-period").dateRangePicker({
	separator : ' to ',
	getValue: function()
	{
		if ($('#date-from').val() && $('#date-to').val() )
			return $('#date-from').val() + ' to ' + $('#date-to').val();
		else
			return '';
	},
	setValue: function(s,s1,s2)
	{
		$('#date-from').val(s1);
		$('#date-to').val(s2);
	}
});
